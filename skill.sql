/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.28 : Database - begincode
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`begincode` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `begincode`;

/*Table structure for table `begincode_skill` */

DROP TABLE IF EXISTS `begincode_skill`;

CREATE TABLE `begincode_skill` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(50) NOT NULL,
  `delete_flag` char(1) DEFAULT NULL,
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `begincode_skill` */

insert  into `begincode_skill`(`skill_id`,`skill_name`,`delete_flag`) values (1,'java','1'),(2,'maven','1'),(3,'app','1'),(4,'all','1'),(5,'jsp','1'),(6,'mysql','1'),(7,'skill','0');

/*Table structure for table `begincode_user` */

DROP TABLE IF EXISTS `begincode_user`;

CREATE TABLE `begincode_user` (
  `begincode_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(100) NOT NULL,
  `nickname` varchar(100) NOT NULL,
  PRIMARY KEY (`begincode_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `begincode_user` */

insert  into `begincode_user`(`begincode_user_id`,`login_name`,`nickname`) values (1,'zhang7','hah'),(2,'yang','hh3'),(3,'gu','ss');

/*Table structure for table `begincode_user_skill` */

DROP TABLE IF EXISTS `begincode_user_skill`;

CREATE TABLE `begincode_user_skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL,
  `begincode_user_id` int(11) NOT NULL,
  `mark` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `skill_id` (`skill_id`),
  KEY `begincode_user_id` (`begincode_user_id`),
  CONSTRAINT `begincode_user_skill_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `begincode_skill` (`skill_id`),
  CONSTRAINT `begincode_user_skill_ibfk_2` FOREIGN KEY (`begincode_user_id`) REFERENCES `begincode_user` (`begincode_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `begincode_user_skill` */

insert  into `begincode_user_skill`(`id`,`skill_id`,`begincode_user_id`,`mark`) values (21,1,1,10),(22,1,2,6),(23,1,3,3),(24,2,1,2),(25,2,2,5),(26,2,3,10),(27,3,1,3),(28,3,2,4),(29,3,3,7),(30,5,1,4),(31,5,2,3),(33,5,3,4),(34,6,1,5),(35,6,2,2),(36,6,3,1),(40,4,1,6),(41,4,2,1),(42,4,3,3);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
