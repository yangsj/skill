package org.begincode.skill.context;

/**
 * Created by saber on 2016/8/5 0005.
 */
import org.begincode.skill.model.BegincodeSkill;
import org.begincode.skill.model.BegincodeUser;
import org.begincode.skill.model.BegincodeUserSkill;
import org.begincode.skill.service.UserSkillService;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:applicationContext-skill.xml"})
public class BaseTest extends AbstractJUnit4SpringContextTests {
    @Resource
    private UserSkillService userSkillService;
    @BeforeClass
    public static void test(){
    }
    @Test
    public void testSelMarkById(){
        BegincodeUserSkill begincodeUserSkill = new BegincodeUserSkill();
        begincodeUserSkill.setBegincodeUserId(1);
        List<BegincodeUserSkill> list = userSkillService.selMarkById(begincodeUserSkill.getBegincodeUserId());
        for(int i = 0;i < list.size();i++){
            System.out.println("userMark:"+ list.get(i).getMark());
        }
    }

    @Test
    public void testSelUserById(){
        System.out.println("userName:"+userSkillService.selUserById(1).getLoginName());
    }

    @Test
    public void testUpdateMarkById() {
        BegincodeUserSkill begincodeUserSkill = new BegincodeUserSkill();
        int[] skillMark = {1, 2, 3, 4, 5, 6};
        int[] id = {21, 40, 34, 30, 27, 24};
        int[] begincodeUserId = {1, 1, 1, 1, 1, 1};
        userSkillService.updateMarkById(skillMark,id,begincodeUserId);
        System.out.println(String.valueOf(userSkillService.updateMarkById(skillMark,id,begincodeUserId)));
    }

    @Test
    public void testSelAllSkill(){
        List<BegincodeSkill> list = userSkillService.selAllSkill();
        for (int i = 0; i < list.size(); i++){
            list.get(i).getSkillName();
            System.out.println("skillName:"+list.get(i).getSkillName()+"");
        }
    }

    @Test
    public void testSelAllUser(){
        List<BegincodeUser> list = userSkillService.selAllUser();
        for (int i = 0; i < list.size(); i++){
            list.get(i).getLoginName();
            System.out.println("userName:"+list.get(i).getLoginName()+"");
        }

    }

    @Test
    public void testSelAllMark(){
        List<BegincodeUserSkill> list = userSkillService.selAllMark();
        for (int i = 0; i < list.size(); i++){
            list.get(i).getMark();
            System.out.println("mark:"+list.get(i).getMark()+"");
        }
    }

}