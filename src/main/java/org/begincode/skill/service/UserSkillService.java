package org.begincode.skill.service;

import org.begincode.skill.constant.Constant;
import org.begincode.skill.mapper.BegincodeSkillMapper;
import org.begincode.skill.mapper.BegincodeUserMapper;
import org.begincode.skill.mapper.BegincodeUserSkillMapper;
import org.begincode.skill.model.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by saber on 2016/7/21.
 */
@Service
public class UserSkillService {
    @Resource
    private BegincodeUserSkillMapper begincodeUserSkillMapper;
    @Resource
    private BegincodeSkillMapper begincodeSkillMapper;
    @Resource
    private BegincodeUserMapper begincodeUserMapper;
    /**
     * @Description: 用户id查询所有技能分数
     */
    public List<BegincodeUserSkill> selMarkById(int begincodeUserId){
        BegincodeUserSkillExample begincodeUserSkillExample = new BegincodeUserSkillExample();
        begincodeUserSkillExample.createCriteria().andBegincodeUserIdEqualTo(begincodeUserId);
        return begincodeUserSkillMapper.selectByExample(begincodeUserSkillExample);
    }

    /**
     * @Description: 用户id查询用户名
     */
    public BegincodeUser selUserById(int begincodeUserId){
        return begincodeUserMapper.selectByPrimaryKey(begincodeUserId);
    }

    /**
     * @Description: 联合id、用户id更新分数
     */
    public int updateMarkById(int[] skillMark,int[] id ,int[] begincodeUserId){
        List<BegincodeUserSkill> list =new ArrayList<BegincodeUserSkill>();
        for(int a = 0 ; a < id.length; a++) {
            BegincodeUserSkill begincodeUserSkill = new BegincodeUserSkill();
            if(skillMark[a] > 0 && skillMark[a] < 11) {
                begincodeUserSkill.setMark(skillMark[a]);
                begincodeUserSkill.setId(id[a]);
                begincodeUserSkill.setBegincodeUserId(begincodeUserId[a]);
                list.add(a, begincodeUserSkill);
                for(int i = 0;i <list.size(); i++) {
                    begincodeUserSkillMapper.updateByPrimaryKeySelective( list.get(i));
                }
            }else{
                return 0;
            }
        }
        return 1;
    }

    /**
     * @Description: 查询所有技能名称
     */
    public List<BegincodeSkill> selAllSkill(){
        BegincodeSkillExample begincodeSkillExample = new BegincodeSkillExample();
        begincodeSkillExample.createCriteria().andDeleteFlagEqualTo(Constant.NO_DEL_FLAG);
        return begincodeSkillMapper.selectByExample(begincodeSkillExample);
    }

    /**
     * @Description: 查询所有用户名
     */
    public List<BegincodeUser> selAllUser(){
        BegincodeUserExample begincodeUserExample = new BegincodeUserExample();
        begincodeUserExample.createCriteria().andBegincodeUserIdIsNotNull();
        return begincodeUserMapper.selectByExample(begincodeUserExample);
    }

    /**
     * @Description: 查询所有分数
     */
    public List<BegincodeUserSkill> selAllMark(){
        BegincodeUserSkillExample begincodeUserSkillExample = new BegincodeUserSkillExample();
        begincodeUserSkillExample.createCriteria().andIdIsNotNull();
        return begincodeUserSkillMapper.selectByExample(begincodeUserSkillExample);
    }


}
