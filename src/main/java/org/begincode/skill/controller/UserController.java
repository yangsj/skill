package org.begincode.skill.controller;
import org.begincode.skill.model.BegincodeSkill;
import org.begincode.skill.model.BegincodeUser;
import org.begincode.skill.model.BegincodeUserSkill;
import org.begincode.skill.service.UserSkillService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("")
public class UserController {

	@Resource
	private UserSkillService userSkillService;

	@RequestMapping(value="/userSkill" ,method= RequestMethod.POST)
	public String selUserSkill(int begincodeUserId, Model model){
		List<BegincodeSkill> skillName =  userSkillService.selAllSkill();
		List<BegincodeUserSkill> skillMark = userSkillService.selMarkById(begincodeUserId);
		BegincodeUser userName = userSkillService.selUserById(begincodeUserId);
		selAll(model);
		model.addAttribute("skillName",skillName);
		model.addAttribute("skillMark",skillMark);
		model.addAttribute("userName",userName);
		return "page/user_skill";
	}

	@RequestMapping(value="/all" ,method= RequestMethod.GET)
	public String selAll(Model model){
		List<BegincodeSkill> skillList = userSkillService.selAllSkill();
		List<BegincodeUser> userList = userSkillService.selAllUser();
		List<BegincodeUserSkill> markList = userSkillService.selAllMark();
		model.addAttribute("allSkill",skillList);
		model.addAttribute("allUser",userList);
		model.addAttribute("allMark",markList);
		return "page/skill_list";
	}

	@RequestMapping("/input")
	public String updateMark(int[] skillMark,int[] id , int[] begincodeUserId, Model model) {


        int a = userSkillService.updateMarkById(skillMark, id, begincodeUserId);
        if (a == 0) {
            return null;
        }
        int userId = begincodeUserId[0];

        selUserSkill(userId, model);
        return "page/skill";
    }
}
