package org.begincode.skill.constant;

/**
 * Created by saber on 2016/7/20.
 */
public class Constant {
    //系统删除状态常量
    public static final String DEL_FLAG = "0";  //删除状态
    public static final String NO_DEL_FLAG = "1"; //可用状态
}
