package org.begincode.skill.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.begincode.skill.model.BegincodeUser;
import org.begincode.skill.model.BegincodeUserExample;
import org.springframework.stereotype.Repository;

@Repository
public interface BegincodeUserMapper {
    int countByExample(BegincodeUserExample example);

    int deleteByExample(BegincodeUserExample example);

    int deleteByPrimaryKey(Integer begincodeUserId);

    int insert(BegincodeUser record);

    int insertSelective(BegincodeUser record);

    List<BegincodeUser> selectByExample(BegincodeUserExample example);

    BegincodeUser selectByPrimaryKey(Integer begincodeUserId);

    int updateByExampleSelective(@Param("record") BegincodeUser record, @Param("example") BegincodeUserExample example);

    int updateByExample(@Param("record") BegincodeUser record, @Param("example") BegincodeUserExample example);

    int updateByPrimaryKeySelective(BegincodeUser record);

    int updateByPrimaryKey(BegincodeUser record);
}