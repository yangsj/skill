package org.begincode.skill.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.begincode.skill.model.BegincodeSkill;
import org.begincode.skill.model.BegincodeSkillExample;
import org.springframework.stereotype.Repository;

@Repository
public interface BegincodeSkillMapper {
    int countByExample(BegincodeSkillExample example);

    int deleteByExample(BegincodeSkillExample example);

    int deleteByPrimaryKey(Integer skillId);

    int insert(BegincodeSkill record);

    int insertSelective(BegincodeSkill record);

    List<BegincodeSkill> selectByExample(BegincodeSkillExample example);

    BegincodeSkill selectByPrimaryKey(Integer skillId);

    int updateByExampleSelective(@Param("record") BegincodeSkill record, @Param("example") BegincodeSkillExample example);

    int updateByExample(@Param("record") BegincodeSkill record, @Param("example") BegincodeSkillExample example);

    int updateByPrimaryKeySelective(BegincodeSkill record);

    int updateByPrimaryKey(BegincodeSkill record);
}