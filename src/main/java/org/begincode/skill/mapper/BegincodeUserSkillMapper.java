package org.begincode.skill.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.begincode.skill.model.BegincodeUserSkill;
import org.begincode.skill.model.BegincodeUserSkillExample;
import org.springframework.stereotype.Repository;

@Repository
public interface BegincodeUserSkillMapper {
    int countByExample(BegincodeUserSkillExample example);

    int deleteByExample(BegincodeUserSkillExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(BegincodeUserSkill record);

    int insertSelective(BegincodeUserSkill record);

    List<BegincodeUserSkill> selectByExample(BegincodeUserSkillExample example);

    BegincodeUserSkill selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") BegincodeUserSkill record, @Param("example") BegincodeUserSkillExample example);

    int updateByExample(@Param("record") BegincodeUserSkill record, @Param("example") BegincodeUserSkillExample example);

    int updateByPrimaryKeySelective(BegincodeUserSkill record);

    int updateByPrimaryKey(BegincodeUserSkill record);
}