<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: saber
  Date: 2016/7/30
  Time: 上午 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="/js/echarts.js"></script>
</head>
<body>
<div id="main" style="width: auto;height:150px;"></div>
<script type="text/javascript">
    var app = echarts.init(document.getElementById('main'));
    app.title= '技能图';
    var a =[];
    <c:forEach items="${skillName}" var="news">
    a.push({ name: '${news.skillName}', max: [10]})
    </c:forEach>
    var b=[];
    var c=[];
    <c:forEach items="${skillMark}" var="news">
    b.push(${news.mark});
    c.push(0);
    </c:forEach>
    option = {
        tooltip: {},
        radar: {
            indicator: a,
            radius: 40,
            startAngle: 120,
            splitNumber: 5,
        },
        series: [{
            type: 'radar',
            data : [
                {
                    value : c,
                    name : '零点'
                },
                {
                    value : b,
                    name : '技能分数'
                }
            ]
        }]
    };
    app.setOption(option);
</script>
<button class="btn btn-mini" type="button" id="btn01">Click</button>
</div>
</body>
</html>
