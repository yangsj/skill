<%--
  Created by IntelliJ IDEA.
  User: saber
  Date: 2016/7/27
  Time: 下午 7:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../commons/taglibs.jsp"%>

<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <title>BeginCode,初学者论坛，begincode,BeginCode</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="在线教程,begincode www.begincode.net  初学者社区，志在让更多软件开发初学者能够掌握一门开发技术  " />
    <%@ include file="../commons/meta.jsp"%>
    <link href="${ctx}/css/simplePagination.css" rel="stylesheet">
    <link media="screen" href="${ctx}/css/bootstrap.css" rel="stylesheet">
    <link media="screen" href="${ctx}/css/courseList.css" rel="stylesheet">
    <script src="${ctx}/js/jquery.min.js"></script>
    <script src="/js/echarts.js"></script>
</head>
<body>
<jsp:include page="/page/core/top.jsp" />
<div class="jumbotron">
    <div class="container">
        <h1>成员技能列表</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
<div id="main" style="width: 600px;height:400px;"></div>
<!-- 定义js显示图表 -->
<script type="text/javascript">
    var app = echarts.init(document.getElementById('main'));
    app.title = '成员技能图';

    var hours = [];
    <c:forEach items="${allSkill}" var="news">
    hours.push('${news.skillName}');
    </c:forEach>

    var days = [];
    <c:forEach items="${allUser}" var="news">
    days.push('${news.loginName}');
    </c:forEach>

    var data = [];

    <c:forEach items="${allMark}" var="news">
    var a = ${news.begincodeUserId}-1;
    var b = ${news.skillId}-1;
    var c = ${news.mark};
    data.push([ a, b, c]);
    </c:forEach>

    option = {
        tooltip: {
            position: 'top'
        },
        title: [],
        singleAxis: [],
        series: []
    };

    echarts.util.each(days, function (day, idx) {
        option.title.push({
            textBaseline: 'middle',
            top: (idx + 0.5) * 100 / 7 + '%',
            text: day
        });
        option.singleAxis.push({
            left: 150,
            type: 'category',
            boundaryGap: false,
            data: hours,
            top: (idx * 100 / 7 + 5) + '%',
            height: (100 / 7 - 10) + '%',
            axisLabel: {
                interval: 0
            }
        });
        option.series.push({
            singleAxisIndex: idx,
            coordinateSystem: 'singleAxis',
            type: 'scatter',
            data: [],
            symbolSize: function (dataItem) {
                return dataItem[1] * 4;
            }
        });
    });

    echarts.util.each(data, function (dataItem) {
        option.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);
    });
    app.setOption(option);
</script>
    </div>
</div>
<hr>
<jsp:include page="/page/core/foot.jsp" />
<script src="${ctx}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.simplePagination.js"></script>

</body>
</html>