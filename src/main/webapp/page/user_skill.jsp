<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="begincode www.begincode.net  初学者社区，志在让更多软件开发初学者能够掌握一门开发技术  " />
    <%@ include file="/commons/meta.jsp"%>
    <title>经典博文,初学者论坛,BeginCode,beginCode</title>
    <!-- Bootstrap -->
    <link href="${ctx}/css/bootstrap.css" rel="stylesheet">
    <link href="${ctx}/css/simplePagination.css" rel="stylesheet"	type="text/css" />

    <script type="text/javascript" src="${ctx}/js/jquery.simplePagination.js"></script>
    <script src="js/echarts.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>

    <script type="text/javascript">

    $(function() {
//jquery validate 注释name验证
        if ($.validator) {
            //fix: when several input elements shares the same name, but has different id-ies....
            $.validator.prototype.elements = function () {
                var validator = this,
                        rulesCache = {};
                // select all valid inputs inside the form (no submit or reset buttons)
                // workaround $Query([]).add until http://dev.jquery.com/ticket/2114 is solved
                return $([]).add(this.currentForm.elements)
                        .filter(":input")
                        .not(":submit, :reset, :image, [disabled]")
                        .not(this.settings.ignore)
                        .filter(function () {
                            var elementIdentification = this.id || this.name;
                            !elementIdentification && validator.settings.debug && window.console && console.error("%o has no id nor name assigned", this);
                            // select only the first element for each name, and only those with rules specified
                            if (elementIdentification in rulesCache || !validator.objectLength($(this).rules()))
                                return false;
                            rulesCache[elementIdentification] = true;
                            return true;
                        });
            };
        }

        $("#btn01").click(function(){ $("#div01").toggle(); });

        //验证通过提交
        $.validator.setDefaults({

            submitHandler: function () {
                $('#div01').hide("fast");
                jQuery.ajax({
                    url: 'input',
                    data: $('#input').serialize(),
                    type: "POST",
                    success: function (returnedData) {

                        $("#main").html(returnedData);
                    }
                });
                return false;
            }
    });
        //开启验证
        $("#input").validate({

            });



    });
    </script>
    <style type="text/css">
        input.error { border: 1px solid red; }
        label.error {
            background:url("/images/unchecked.gif") no-repeat 0px 0px;

            padding-left: 16px;

            padding-bottom: 2px;

            font-weight: bold;

            color: #EA5200;
        }
        label.checked {
            background:url("/images/checked.gif") no-repeat 0px 0px;
        }
        input{
            border: 1px solid #ccc;
            padding: 7px 0px;
            border-radius: 3px;
            padding-left:5px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s
        }
        input:focus{
            border-color: #66afe9;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
        }


    </style>
</head>
<body>
<jsp:include page="/page/core/top.jsp" />
<ol class="breadcrumb">
    <li class="active"> 首页 </li>
    <li class="active">空间管理</li>
</ol>


<div class="container .col-xs-" style="margin-top:10px; border-radius: 0px;">
    <div class="row" >
        <div class="col-md-3">
            <div class="panel panel-primary" style="margin-bottom:10px">
                <div class="panel-body" >
                    <div class="media" style="margin-bottom:5px">
                        <a class="media-left" href="">
                            <img src="${user.pic}" id="pic" style="width:80px;height:80px;" alt="${user.nickname}">
                        </a>
                        <div class="media-body"  >
                            <h2 class="media-heading " id="nickName" style="color:#EA0000">${user.nickname}</h2>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12" >
                    <div id="main" style="width: auto;height:150px;"></div>
                    <script type="text/javascript">
                        var app = echarts.init(document.getElementById('main'));
                        app.title= '技能图';
                        var a =[];
                        <c:forEach items="${skillName}" var="news">
                        a.push({ name: '${news.skillName}', max: [10]})
                        </c:forEach>
                        var b=[];
                        var c=[];
                        <c:forEach items="${skillMark}" var="news">
                        b.push(${news.mark});
                        c.push(0);
                        </c:forEach>
                        option = {
                            tooltip: {},
                            radar: {
                                indicator: a,
                                radius: 40,
                                startAngle: 120,
                                splitNumber: 5,
                            },
                            series: [{
                                type: 'radar',
                                data : [
                                    {
                                        value : c,
                                        name : '零点'
                                    },
                                    {
                                        value : b,
                                        name : '技能分数'
                                    }
                                ]
                            }]
                        };
                        app.setOption(option);


                    </script>
                    <button class="btn btn-mini" type="button" id="btn01">Click</button>
                </div>
                <div class="col-md-12"id="div01" style="display:none;">
                    <form action="input" method="post" class="STYLE-NAME"  id="input">

                        <h5> <span>根据你的能力给与适当的分数(0-10)</span></h5>

                        <table>
                        <c:forEach items="${skillMark}" var="news" varStatus="temp">
                            <tr>

                                <td>${skillName[temp.count-1].skillName} :</td>
                                <td><input  type="text" id="${news.skillId}" name="skillMark" value="${news.mark}" min="0" max="10"  required digits="true"/></td>
                                   <td> <input  type="hidden" id="${news.id}" name="id" value="${news.id}" /></td>
                                <td> <input  type="hidden" id="${news.begincodeUserId}" name="begincodeUserId" value="${news.begincodeUserId}" /></td>

                            </tr>
                        </c:forEach>
                        <tr>
                            <td><input class="btn" id="inp01" type="submit" value="sumbit"></td>
                        </tr>
                            </table>
                    </form>


                </div>

                <div class="col-md-12">
                    <button type="button" class="btn btn-primary" id="addBlog" onclick="addBlog()" data-toggle="button" aria-pressed="false" autocomplete="off" style="border-radius: 0px;width:100%;height:60px;margin-bottom:10px;font-weight:bold;font-size:35px">
                        发布博文
                    </button>
                </div>
            </div>
            <div class="list-group" id="classList">
                <a href="#" class="list-group-item active"> 博客分类</a>
            </div>
            <div class="list-group" id="readList">
                <a href="#" class="list-group-item active"> 阅读排行 </a>
            </div>
        </div>

        <div class="col-md-9" >
            <div class="panel panel-primary" style="margin-top:0;" >
                <div class="panel-body" >
                    <ol class="breadcrumb" style="background-color:#5bc0de;">
                        <li class="active"> 空间 </li>
                        <li class="active"> 博客</li>
                        <li class="active">分类</li>
                    </ol >
                    <ul class="list-group" style="margin-bottom:10px;" id="blogsList" >
                        <c:forEach items="${blogs}" var="blog">
                            <li class="list-group-item">
                                <h3 style="color:#5bc0de"><a href="${ctx}/blog/${blog.blogId}">${blog.blogInfo }</a></h3>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;${blog.blogAbstract } </p>
                                <div class="row">
                                    <div  class="col-md-6">
                                        <p style="text-align: left;">分类名称：${blog.blogTypeName}</p>
                                    </div>
                                    <div  class="col-md-6">
                                        <p style="text-align: right;"><a href="${ctx}/blog/blogId/${blog.blogId }"><b>编辑</b></a>&nbsp; | &nbsp;<b>阅读</b>(${blog.viewCount})&nbsp;  </p>

                                    </div>
                                </div>

                            </li>
                        </c:forEach>

                    </ul>
                    <div class="row center"  >
                        <div id="pagediv"  ></div>
                    </div>
                </div></div>
        </div>
    </div>
</div>
<input type="hidden" name="blogTypeId" id="blogTypeId" value="${blogTypeId}" />
<hr>
<!-- foot -->
<jsp:include page="/page/core/foot.jsp" />
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- msg end -->
<script src="${ctx}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${ctx}/js/blog/blogUtil.js"></script>
<script type="text/javascript" src="${ctx}/js/blog/blogUser.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery.cookie.js"></script>
<script type="text/javascript">
    $(document).ready(function(e) {
        $("#pagediv").pagination({
            items : ${pageinfo.totalCount},
            itemsOnPage : 10,
            cssStyle : 'light-theme',
            onPageClick : changeBlogUser
        });
        var ctx = "${ctx}";
        userTopTen();
        blogTypes();
    });
</script>
</body>
</html>